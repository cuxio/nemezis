"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorType = exports.ErrorCode = void 0;
var ErrorCode;
(function (ErrorCode) {
    ErrorCode[ErrorCode["NOT_FOUND"] = 404] = "NOT_FOUND";
    ErrorCode[ErrorCode["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    ErrorCode[ErrorCode["BAD_REQUEST"] = 400] = "BAD_REQUEST";
})(ErrorCode = exports.ErrorCode || (exports.ErrorCode = {}));
var ErrorType;
(function (ErrorType) {
    ErrorType["MONGO"] = "mongo";
    ErrorType["CUSTOM"] = "custom";
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
