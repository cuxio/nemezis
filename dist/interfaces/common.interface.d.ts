export interface ObjectProps {
    [name: string]: boolean | number | ObjectProps | string;
}
