import { WebSocketError } from './websocket.interface';
export declare const isWebSocketError: (data: unknown | Pick<WebSocketError, 'code'>) => data is Omit<WebSocketError, "messageData">;
