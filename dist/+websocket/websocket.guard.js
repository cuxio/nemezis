"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isWebSocketError = void 0;
var isWebSocketError = function (data) {
    return !!data && data.code !== undefined;
};
exports.isWebSocketError = isWebSocketError;
