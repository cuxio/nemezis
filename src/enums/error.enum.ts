export enum ErrorCode {
  NOT_FOUND = 404,
  UNAUTHORIZED = 401,
  BAD_REQUEST = 400,
}

export enum ErrorType {
  MONGO = 'mongo',
  CUSTOM = 'custom',
}
