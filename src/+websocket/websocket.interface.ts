import { WebSocketActionName } from './websocket.enum';
import { ErrorCode, ErrorType } from '../enums';

export interface WebSocketBaseMessage {
  readonly actionName: WebSocketActionName;
  readonly projectSlug: string;
  readonly token: string;
  readonly id: string;
  isChain?: boolean;
  isNewAction?: boolean;
  isPublic?: boolean;
  noProject?: boolean;
  projectsSlug?: string[];
  timeout?: number;
}

export interface WebSocketParams {
  filter?: string;
  page?: number;
  items?: number;
  pathname?: string;
}

export type WebSocketMessage = WebSocketBaseMessage & WebSocketParams;

export interface WebSocketAnswer<T> {
  _: {
    queryTime: number;
    ts: number;
  };
  messageData: WebSocketMessage;
  response: T;
}

export interface WebSocketError {
  code: ErrorCode | number;
  messageData: WebSocketMessage;
  reason: string;
  type: ErrorType;
  isIntegrated?: boolean;
  shouldRefreshToken?: boolean;
  tip?: string;
}

export type WebSocketResponse<T> = WebSocketAnswer<T> | WebSocketError;
