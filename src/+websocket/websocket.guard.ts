import { WebSocketError } from './websocket.interface';

export const isWebSocketError = (
  data: unknown | Pick<WebSocketError, 'code'>,
): data is Omit<WebSocketError, 'messageData'> =>
  !!data && (data as WebSocketError).code !== undefined;
