### What is this repository for? ###
This repository contains shared websocket action names and interfaces.

## Instructions
Whenever you want to add new action in aggregator, then you have to update Nemezis so both UI and Aggregator will share this enum.

1. Make a change in Nemezis (e.x add new action)
2. (Important) You have to build package and commit dist directory to repository. Execute `npm run build`.
3. Commit, create pull request and merge to master.
4. Now, you are able to use these changes in other applications.
```
npm uninstall @cux/nemezis
npm install --save @cux/nemezis@git+ssh://git@bitbucket.org/cuxio/nemezis.git
```
or
```
yarn remove @cux/nemezis
yarn add @cux/nemezis@git+ssh://git@bitbucket.org/cuxio/nemezis.git
```
